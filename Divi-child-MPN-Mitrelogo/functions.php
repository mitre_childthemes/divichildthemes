<?php
function divi_child_theme_enqueue_styles() {

    $parent_style = 'divi-style'; // This is 'divi-style' for the Divi theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'divi-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'divi_child_theme_enqueue_styles' );

if( ! function_exists('et_get_footer_credits') ) :
    function et_get_footer_credits() {
    
        $credits_format = '<%2$s id="footer-info">%1$s</%2$s>';
        $date = date("Y");
        $home_url = esc_url(home_url('/'));
        $footer_credits = 'Copyright &copy; %1$s <a href="%2$s" rel="home"> %3$s - MITRE Corporation</a></br>
        The MITRE Corporation. All rights reserved. MITRE is a registered trademark of The MITRE Corporation. Material on this site may be copied and distributed with permission only.';
        $format_footer = sprintf($footer_credits, $date, $home_url, get_bloginfo( 'name' ));
        return et_get_safe_localization( sprintf( $credits_format, $format_footer, 'p' ) );
    }
endif;

add_filter('wp_generate_tag_cloud', 'filter_tag_cloud', 10, 1);
function filter_tag_cloud($string){
   return str_replace("pt", "pt !important", $string);
}

function get_mitre_logo() {
	$logo = ($user_logo = get_attachment_url_by_slug('mitre_logo')) && $user_logo != '' 
        ? $user_logo
        : get_stylesheet_directory_uri() . '/mitre_logo.png';
    
    echo '<a href="https://www.mitre.org/" target="_blank">';
        echo '<img src="' . esc_attr( $logo ) . '" alt="' . esc_attr( get_bloginfo( 'name' )) . '" id="logo" data-height-percentage="' . esc_attr( et_get_option( 'logo_height', '54' ) ) . '" />';
    echo '</a>';
}

function get_attachment_url_by_slug( $slug ) {
    $args = array(
      'post_type' => 'attachment',
      'name' => sanitize_title($slug),
      'posts_per_page' => 1,
      'post_status' => 'inherit',
    );
    $_header = get_posts( $args );
    $header = $_header ? array_pop($_header) : null;
    return $header ? wp_get_attachment_url($header->ID) : '';
  }

function get_mpn_header() {
    echo '<div id="mpn-40355615407952805-wrapper">';
        echo '<div id="mpn-40355615407952805-bg"></div>';
            echo '<div id="mpn-40355615407952805-header">';
                echo '<div class="mpn-40355615407952805-left">';
                    echo '<img src="https://mpn.mitre.org/static/header//images/shim.gif" alt="" width="0" height="10" >';
                    echo '<a href="https://mpn.mitre.org" data-slimstat="1">';
                        echo '<img class="mpn-40355615407952805-phl" src="https://mpn.mitre.org/static/header//images/text_mpn.png" width="33" height="11" >';
                        echo '<img class="mpn-40355615407952805-not-mobile" src="https://mpn.mitre.org/static/header//images/text_mitre.png" width="41" height="10" >';
                        echo '<img class="mpn-40355615407952805-not-mobile" src="https://mpn.mitre.org/static/header//images/text_partnership_network.png" width="167" height="10" >';
                    // EMBreadCrumbMPNHeaderPatch 2
                    echo '</a>';
                echo '</div>';
                echo '<div class="mpn-40355615407952805-right">';
                    echo '<div>';
                        echo '<a href="http://www.mitre.org/" data-slimstat="1">';
                            echo '<img src="https://mpn.mitre.org/static/header/images/external_mitre_logo.png" width="142" height="34" >';
                        echo '</a>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';
}

?>